<?php

namespace App\Helpers\Contracts;


Interface Exporter
{
	public function export($columns, $data);
}
