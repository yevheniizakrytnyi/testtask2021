<?php

namespace App\Services;

use App\Exports\Formats\ExportCSV;
use App\Exports\Formats\ExportXML;

class FileExportService
{

    /**
     * @var ExportXML
     */
    private $exportXML;

    /**
     * @var ExportCSV
     */
    private $exportCSV;


    public function __construct(ExportCSV $exportCSV, ExportXML $exportXML)
    {
        $this->exportCSV = $exportCSV;
        $this->exportXML = $exportXML;
    }

    public function exportToCsv($exportData)
    {
        return $this->exportCSV->export($exportData['columns'], $exportData['data']);
    }

    public function exportToXml($exportData)
    {
        return $this->exportXML->export($exportData['columns'], $exportData['data']);
    }
}
