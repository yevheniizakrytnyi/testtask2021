<?php

namespace App\Exports\Formats;

use App\Helpers\Contracts\Exporter;

class ExportXML implements Exporter
{

    public function export($columns, $data)
    {
        return view('export', compact('columns', 'data'));
    }
}
