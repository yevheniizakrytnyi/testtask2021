<?php

namespace App\Exports\Formats;

use App\Helpers\Contracts\Exporter;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Exports\FileExport;

class ExportCSV implements Exporter
{

    public function export($columns, $data)
    {
        $data = $this->setColumnsToExportData($columns, $data);
        $data = collect($data);

        $filename = "export.csv";

        return Excel::download(app()->makeWith(FileExport::class, compact('data')), $filename, 'Csv');
    }

    /**
     * Sets the column for an exporting data
     * @param $columns
     * @param $data
     * @return array $data
     */
    public function setColumnsToExportData($columns, $data): array
    {
        $csvColumns = [];
        foreach ($columns as $key => $column) {
            $csvColumns[$column['key']] = $column['key'];
        }

        array_unshift($data, $csvColumns);

        return $data;
    }
}
