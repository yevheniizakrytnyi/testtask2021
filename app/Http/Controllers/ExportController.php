<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\ExportRequest;
use App\Services\FileExportService;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExportController extends Controller {

    private $fileExportService;

    public function __construct(FileExportService $fileExportService)
    {
        $this->fileExportService = $fileExportService;
    }

    /**
     * Converts the user input into a CSV
     * @param ExportRequest $request
     * @return BinaryFileResponse|void
     */
    public function convertToCsv(ExportRequest $request)
    {
        return $this->fileExportService->exportToCsv($request->validated());
    }

    /**
     * Converts the user input into a XML
     * @param ExportRequest $request
     * @return View
     */
    public function convertToXml(ExportRequest $request)
    {
        return $this->fileExportService->exportToXml($request->validated());
    }

}
