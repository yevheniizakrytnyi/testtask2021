import axios from 'axios';

class Api {
    constructor() {
        this.axios = axios.create({
            baseURL: '/api',
            timeout: 300000,
            headers: {
                'Accept': 'application/json'
            }
        });
    }

    async exportToCsv(columns, data) {
        const endpoint = '/api/export-to-csv';
        return await axios.post(endpoint, {
            data: data,
            columns: columns,
        });
    }

    async exportToXml(columns, data) {
        const endpoint = '/api/export-to-xml';
        return await axios.post(endpoint, {
            columns: columns,
            data: data
        });
    }
}

export default new Api();
