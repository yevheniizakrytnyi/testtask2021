export default {
    methods: {
        downloadBinaryFile(response, filename) {
            const data = response.data ? response.data : response;
            const url = window.URL.createObjectURL(new Blob([data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
            link.remove();
        }
    }
}
