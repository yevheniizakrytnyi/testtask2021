<?xml version="1.0" encoding="UTF-8"?>
<root>
    <columns>
        @foreach($columns as $column)
            <column>{{ $column['key'] }}</column>
        @endforeach
    </columns>
    <items>
        @foreach($data as $key => $row)
            <item>
                @foreach($row as $columnName => $columnValue)
                    <{{ $columnName }}>{{ $columnValue }}</{{ $columnName }}>
                @endforeach
            </item>
        @endforeach
    </items>
</root>
