<?php

namespace Tests\Feature;

use Symfony\Component\HttpFoundation\File\File;
use Tests\TestCase;

class ConvertToCsvTest extends TestCase
{

    /** @test */
    public function can_export_to_csv_test()
    {
        $columns = [['key' => 'firstName'], ['key' => 'lastName'], ['key' =>'emailAddress']];

        $values = [
            ['firstName' => 'John', 'lastName' => 'Doe', 'emailAddress' => 'john.doe@example.com'],
            ['firstName' => 'Jack', 'lastName' => 'Smith', 'emailAddress' => 'jack.smith@example.com']
        ];

        $data = [
            'columns' => $columns,
            'data' => $values,
        ];

        // Can export data if CSV
        /** @var File $allOffersFile */
        $csvFile = $this->apiCall('export-to-csv', 'post', $data)->assertOk()->getFile();
        $csv = array_map('str_getcsv', file($csvFile->getPathname()));
        $this->assertNotEmpty($csv);
        $this->assertTrue(count($csv) == 3);
    }

    /** @test */
    public function can_export_to_csv_without_data_test()
    {
        $columns = [['key' => 'firstName'], ['key' => 'lastName'], ['key' =>'emailAddress']];

        $data = [
            'columns' => $columns,
            'data' => []
        ];

        // Can export data if CSV
        /** @var File $allOffersFile */
        $csvFile = $this->apiCall('export-to-csv', 'post', $data)->assertOk()->getFile();
        $csv = array_map('str_getcsv', file($csvFile->getPathname()));
        $this->assertNotEmpty($csv);
        $this->assertTrue(count($csv) == 1);
    }

    /** @test */
    public function cannot_export_to_csv_without_columns_test()
    {
        $values = [
            ['firstName' => 'John', 'lastName' => 'Doe', 'emailAddress' => 'john.doe@example.com'],
            ['firstName' => 'Jack', 'lastName' => 'Smith', 'emailAddress' => 'jack.smith@example.com']
        ];

        $data = [
            'columns' => [],
            'data' => $values,
        ];

        // Can export data if CSV
        /** @var File $allOffersFile */
        $this->apiCall('export-to-csv', 'post', $data)->assertStatus(422);
    }
}
