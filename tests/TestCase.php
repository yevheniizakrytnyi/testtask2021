<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @param $url
     * @param $method
     * @param array $data
     * @return TestResponse
     */
    protected function apiCall($url, $method, array $data = [])
    {
        $method = "{$method}Json";

        return $this->$method("/api/$url", $data);
    }
}
